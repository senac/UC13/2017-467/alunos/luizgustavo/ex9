/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Principal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LuizGM
 */
public class Teste {
    
    public Teste() {
    }
    @Test
    public void CalcularDistancia(){
        Principal p = new Principal();
        double resultado = p.calcDistancia(2, 100);
        assertEquals(200, resultado, 0.01);
    }
    @Test
    public void CalcularQuantidadeLitros(){
        Principal p = new Principal();
        double resultado = p.litrosUsados(2, 120);
        assertEquals(20, resultado, 0.01);
    }

}
