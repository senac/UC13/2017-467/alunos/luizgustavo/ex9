/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/*  
Efetuar o cálculo da quantidade de litros de combustível gasta em uma viagem, 
utilizando um automóvel que faz 12 Km por litro. Para obter o cálculo,
o usuário deve fornecer o tempo gasto na viagem e a velocidade média. 
Desta forma, será possível obter a distância percorrida com a fórmula DISTANCIA = TEMPO * VELOCIDADE. Tendo o valor da distância, 
basta calcular a quantidade de litros de combustível utilizada na viagem com a fórmula: LITROS_USADOS = DISTANCIA / 12.
O programa deve apresentar os valores da velocidade média, tempo gasto, a distância percorrida e a quantidade de litros utilizada na viagem.
 */
public class Principal {
    public static final double MEDIA = 12;
    
    
    double distancia;
    double litrosUsados;

    public Principal(){
    }
    
    public double calcDistancia(double tempo , double velocidade){
        this.distancia = tempo * velocidade;
        return distancia;
    }
    
    public double litrosUsados(double tempo, double velocidade){
        double resultado = calcDistancia(tempo, velocidade) / MEDIA;
        return  resultado;
    }
}
